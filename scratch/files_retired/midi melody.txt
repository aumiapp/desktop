MIDI MELODY
This instrument allows you to play midi notes on the computer’s built-in Midi bank.

There are two modes: “Zones” or “Sequence”. “Zones” will be trigger notes mapped to the zone divisions, while “Sequence” will play the notes in sequence no matter which zone is triggered. This allows you to sequence melody phrases. To change between modes click the mode button.

You can also input your own pitches. This can be done in three different ways:

1) Entering a sequence by clicking “New Sequence” and playing the desired notes on the keyboard

2) Clicking a black note slot and assigning a specific note.

3) Choosing between one of the available scales and defining its root pitch and octave.
