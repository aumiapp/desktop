{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 7,
			"minor" : 3,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"rect" : [ 1835.0, 96.0, 1092.0, 933.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"boxes" : [ 			{
				"box" : 				{
					"id" : "obj-39",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 702.666687, 203.0, 79.0, 20.0 ],
					"style" : "",
					"text" : "route symbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-32",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 527.0, 340.0, 55.0, 20.0 ],
					"style" : "",
					"text" : "pack s s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-30",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 478.0, 81.0, 33.0, 20.0 ],
					"style" : "",
					"text" : "t s s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-20",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "", "", "" ],
					"patching_rect" : [ 478.0, 340.0, 43.0, 20.0 ],
					"style" : "",
					"text" : "unjoin"
				}

			}
, 			{
				"box" : 				{
					"coll_data" : 					{
						"count" : 128,
						"data" : [ 							{
								"key" : 1,
								"value" : [ "Acoustic", "Grand", "Piano" ]
							}
, 							{
								"key" : 2,
								"value" : [ "Bright", "Acoustic", "Piano" ]
							}
, 							{
								"key" : 3,
								"value" : [ "Electric", "Grand", "Piano" ]
							}
, 							{
								"key" : 4,
								"value" : [ "Honky-tonk", "Piano" ]
							}
, 							{
								"key" : 5,
								"value" : [ "Electric", "Piano", 1 ]
							}
, 							{
								"key" : 6,
								"value" : [ "Electric", "Piano", 2 ]
							}
, 							{
								"key" : 7,
								"value" : [ "Harpsichord" ]
							}
, 							{
								"key" : 8,
								"value" : [ "Clavi" ]
							}
, 							{
								"key" : 9,
								"value" : [ "Celesta" ]
							}
, 							{
								"key" : 10,
								"value" : [ "Glockenspiel" ]
							}
, 							{
								"key" : 11,
								"value" : [ "Music", "Box" ]
							}
, 							{
								"key" : 12,
								"value" : [ "Vibraphone" ]
							}
, 							{
								"key" : 13,
								"value" : [ "Marimba" ]
							}
, 							{
								"key" : 14,
								"value" : [ "Xylophone" ]
							}
, 							{
								"key" : 15,
								"value" : [ "Tubular", "Bells" ]
							}
, 							{
								"key" : 16,
								"value" : [ "Dulcimer" ]
							}
, 							{
								"key" : 17,
								"value" : [ "Drawbar", "Organ" ]
							}
, 							{
								"key" : 18,
								"value" : [ "Percussive", "Organ" ]
							}
, 							{
								"key" : 19,
								"value" : [ "Rock", "Organ" ]
							}
, 							{
								"key" : 20,
								"value" : [ "Church", "Organ" ]
							}
, 							{
								"key" : 21,
								"value" : [ "Reed", "Organ" ]
							}
, 							{
								"key" : 22,
								"value" : [ "Accordion" ]
							}
, 							{
								"key" : 23,
								"value" : [ "Harmonica" ]
							}
, 							{
								"key" : 24,
								"value" : [ "Tango", "Accordion" ]
							}
, 							{
								"key" : 25,
								"value" : [ "Acoustic", "Guitar", "(nylon)" ]
							}
, 							{
								"key" : 26,
								"value" : [ "Acoustic", "Guitar", "(steel)" ]
							}
, 							{
								"key" : 27,
								"value" : [ "Electric", "Guitar", "(jazz)" ]
							}
, 							{
								"key" : 28,
								"value" : [ "Electric", "Guitar", "(clean)" ]
							}
, 							{
								"key" : 29,
								"value" : [ "Electric", "Guitar", "(muted)" ]
							}
, 							{
								"key" : 30,
								"value" : [ "Overdriven", "Guitar" ]
							}
, 							{
								"key" : 31,
								"value" : [ "Distortion", "Guitar" ]
							}
, 							{
								"key" : 32,
								"value" : [ "Guitar", "harmonics" ]
							}
, 							{
								"key" : 33,
								"value" : [ "Acoustic", "Bass" ]
							}
, 							{
								"key" : 34,
								"value" : [ "Electric", "Bass", "(finger)" ]
							}
, 							{
								"key" : 35,
								"value" : [ "Electric", "Bass", "(pick)" ]
							}
, 							{
								"key" : 36,
								"value" : [ "Fretless", "Bass" ]
							}
, 							{
								"key" : 37,
								"value" : [ "Slap", "Bass", 1 ]
							}
, 							{
								"key" : 38,
								"value" : [ "Slap", "Bass", 2 ]
							}
, 							{
								"key" : 39,
								"value" : [ "Synth", "Bass", 1 ]
							}
, 							{
								"key" : 40,
								"value" : [ "Synth", "Bass", 2 ]
							}
, 							{
								"key" : 41,
								"value" : [ "Violin" ]
							}
, 							{
								"key" : 42,
								"value" : [ "Viola" ]
							}
, 							{
								"key" : 43,
								"value" : [ "Cello" ]
							}
, 							{
								"key" : 44,
								"value" : [ "Contrabass" ]
							}
, 							{
								"key" : 45,
								"value" : [ "Tremolo", "Strings" ]
							}
, 							{
								"key" : 46,
								"value" : [ "Pizzicato", "Strings" ]
							}
, 							{
								"key" : 47,
								"value" : [ "Orchestral", "Harp" ]
							}
, 							{
								"key" : 48,
								"value" : [ "Timpani" ]
							}
, 							{
								"key" : 49,
								"value" : [ "String", "Ensemble", 1 ]
							}
, 							{
								"key" : 50,
								"value" : [ "String", "Ensemble", 2 ]
							}
, 							{
								"key" : 51,
								"value" : [ "SynthStrings", 1 ]
							}
, 							{
								"key" : 52,
								"value" : [ "SynthStrings", 2 ]
							}
, 							{
								"key" : 53,
								"value" : [ "Choir", "Aahs" ]
							}
, 							{
								"key" : 54,
								"value" : [ "Voice", "Oohs" ]
							}
, 							{
								"key" : 55,
								"value" : [ "Synth", "Voice" ]
							}
, 							{
								"key" : 56,
								"value" : [ "Orchestra", "Hit" ]
							}
, 							{
								"key" : 57,
								"value" : [ "Trumpet" ]
							}
, 							{
								"key" : 58,
								"value" : [ "Trombone" ]
							}
, 							{
								"key" : 59,
								"value" : [ "Tuba" ]
							}
, 							{
								"key" : 60,
								"value" : [ "Muted", "Trumpet" ]
							}
, 							{
								"key" : 61,
								"value" : [ "French", "Horn" ]
							}
, 							{
								"key" : 62,
								"value" : [ "Brass", "Section" ]
							}
, 							{
								"key" : 63,
								"value" : [ "SynthBrass", 1 ]
							}
, 							{
								"key" : 64,
								"value" : [ "SynthBrass", 2 ]
							}
, 							{
								"key" : 65,
								"value" : [ "Soprano", "Sax" ]
							}
, 							{
								"key" : 66,
								"value" : [ "Alto", "Sax" ]
							}
, 							{
								"key" : 67,
								"value" : [ "Tenor", "Sax" ]
							}
, 							{
								"key" : 68,
								"value" : [ "Baritone", "Sax" ]
							}
, 							{
								"key" : 69,
								"value" : [ "Oboe" ]
							}
, 							{
								"key" : 70,
								"value" : [ "English", "Horn" ]
							}
, 							{
								"key" : 71,
								"value" : [ "Bassoon" ]
							}
, 							{
								"key" : 72,
								"value" : [ "Clarinet" ]
							}
, 							{
								"key" : 73,
								"value" : [ "Piccolo" ]
							}
, 							{
								"key" : 74,
								"value" : [ "Flute" ]
							}
, 							{
								"key" : 75,
								"value" : [ "Recorder" ]
							}
, 							{
								"key" : 76,
								"value" : [ "Pan", "Flute" ]
							}
, 							{
								"key" : 77,
								"value" : [ "Blown", "Bottle" ]
							}
, 							{
								"key" : 78,
								"value" : [ "Shakuhachi" ]
							}
, 							{
								"key" : 79,
								"value" : [ "Whistle" ]
							}
, 							{
								"key" : 80,
								"value" : [ "Ocarina" ]
							}
, 							{
								"key" : 81,
								"value" : [ "Lead", 1, "(square)" ]
							}
, 							{
								"key" : 82,
								"value" : [ "Lead", 2, "(sawtooth)" ]
							}
, 							{
								"key" : 83,
								"value" : [ "Lead", 3, "(calliope)" ]
							}
, 							{
								"key" : 84,
								"value" : [ "Lead", 4, "(chiff)" ]
							}
, 							{
								"key" : 85,
								"value" : [ "Lead", 5, "(charang)" ]
							}
, 							{
								"key" : 86,
								"value" : [ "Lead", 6, "(voice)" ]
							}
, 							{
								"key" : 87,
								"value" : [ "Lead", 7, "(fifths)" ]
							}
, 							{
								"key" : 88,
								"value" : [ "Lead", 8, "(bass", "+", "lead)" ]
							}
, 							{
								"key" : 89,
								"value" : [ "Pad", 1, "(new", "age)" ]
							}
, 							{
								"key" : 90,
								"value" : [ "Pad", 2, "(warm)" ]
							}
, 							{
								"key" : 91,
								"value" : [ "Pad", 3, "(polysynth)" ]
							}
, 							{
								"key" : 92,
								"value" : [ "Pad", 4, "(choir)" ]
							}
, 							{
								"key" : 93,
								"value" : [ "Pad", 5, "(bowed)" ]
							}
, 							{
								"key" : 94,
								"value" : [ "Pad", 6, "(metallic)" ]
							}
, 							{
								"key" : 95,
								"value" : [ "Pad", 7, "(halo)" ]
							}
, 							{
								"key" : 96,
								"value" : [ "Pad", 8, "(sweep)" ]
							}
, 							{
								"key" : 97,
								"value" : [ "FX", 1, "(rain)" ]
							}
, 							{
								"key" : 98,
								"value" : [ "FX", 2, "(soundtrack)" ]
							}
, 							{
								"key" : 99,
								"value" : [ "FX", 3, "(crystal)" ]
							}
, 							{
								"key" : 100,
								"value" : [ "FX", 4, "(atmosphere)" ]
							}
, 							{
								"key" : 101,
								"value" : [ "FX", 5, "(brightness)" ]
							}
, 							{
								"key" : 102,
								"value" : [ "FX", 6, "(goblins)" ]
							}
, 							{
								"key" : 103,
								"value" : [ "FX", 7, "(echoes)" ]
							}
, 							{
								"key" : 104,
								"value" : [ "FX", 8, "(sci-fi)" ]
							}
, 							{
								"key" : 105,
								"value" : [ "Sitar" ]
							}
, 							{
								"key" : 106,
								"value" : [ "Banjo" ]
							}
, 							{
								"key" : 107,
								"value" : [ "Shamisen" ]
							}
, 							{
								"key" : 108,
								"value" : [ "Koto" ]
							}
, 							{
								"key" : 109,
								"value" : [ "Kalimba" ]
							}
, 							{
								"key" : 110,
								"value" : [ "Bag", "pipe" ]
							}
, 							{
								"key" : 111,
								"value" : [ "Fiddle" ]
							}
, 							{
								"key" : 112,
								"value" : [ "Shanai" ]
							}
, 							{
								"key" : 113,
								"value" : [ "Tinkle", "Bell" ]
							}
, 							{
								"key" : 114,
								"value" : [ "Agogo" ]
							}
, 							{
								"key" : 115,
								"value" : [ "Steel", "Drums" ]
							}
, 							{
								"key" : 116,
								"value" : [ "Woodblock" ]
							}
, 							{
								"key" : 117,
								"value" : [ "Taiko", "Drum" ]
							}
, 							{
								"key" : 118,
								"value" : [ "Melodic", "Tom" ]
							}
, 							{
								"key" : 119,
								"value" : [ "Synth", "Drum" ]
							}
, 							{
								"key" : 120,
								"value" : [ "Reverse", "Cymbal" ]
							}
, 							{
								"key" : 121,
								"value" : [ "Guitar", "Fret", "Noise" ]
							}
, 							{
								"key" : 122,
								"value" : [ "Breath", "Noise" ]
							}
, 							{
								"key" : 123,
								"value" : [ "Seashore" ]
							}
, 							{
								"key" : 124,
								"value" : [ "Bird", "Tweet" ]
							}
, 							{
								"key" : 125,
								"value" : [ "Telephone", "Ring" ]
							}
, 							{
								"key" : 126,
								"value" : [ "Helicopter" ]
							}
, 							{
								"key" : 127,
								"value" : [ "Applause" ]
							}
, 							{
								"key" : 128,
								"value" : [ "Gunshot" ]
							}
 ]
					}
,
					"id" : "obj-10",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 702.666687, 175.0, 117.0, 20.0 ],
					"saved_object_attributes" : 					{
						"embed" : 1
					}
,
					"style" : "",
					"text" : "coll GM-instruments"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-7",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 4,
					"outlettype" : [ "int", "bang", "dump", "int" ],
					"patching_rect" : [ 664.0, 139.0, 77.0, 20.0 ],
					"style" : "",
					"text" : "t 0 b dump 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 578.0, 139.0, 34.0, 20.0 ],
					"style" : "",
					"text" : "t b 3"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-70",
					"maxclass" : "number",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 204.0, 556.0, 50.0, 20.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-67",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 86.599998, 259.0, 141.0, 20.0 ],
					"style" : "",
					"text" : "insert 0 (Default sounds)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-63",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 620.0, 235.0, 96.0, 20.0 ],
					"style" : "",
					"text" : "prepend append"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-62",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 620.0, 203.0, 57.0, 20.0 ],
					"style" : "",
					"text" : "strippath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-61",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 578.0, 203.0, 33.0, 20.0 ],
					"style" : "",
					"text" : "t s s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-60",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "bang" ],
					"patching_rect" : [ 578.0, 175.0, 92.0, 20.0 ],
					"style" : "",
					"text" : "opendialog fold"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-55",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 527.0, 369.0, 76.0, 20.0 ],
					"style" : "",
					"text" : "s instrument"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Trebuchet MS",
					"fontsize" : 16.0,
					"id" : "obj-53",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 652.0, 269.0, 96.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 460.0, 17.0, 96.0, 25.0 ],
					"style" : "",
					"text" : "Instrument:",
					"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-22",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 492.0, 308.0, 161.0, 20.0 ],
					"style" : "",
					"text" : "clearchecks, checkitem $1 1"
				}

			}
, 			{
				"box" : 				{
					"autopopulate" : 1,
					"bgcolor" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
					"bgfillcolor_color1" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
					"bgfillcolor_color2" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"fontname" : "Trebuchet MS",
					"fontsize" : 16.0,
					"hint" : "Choose the instrument or sample library to load. ",
					"id" : "obj-54",
					"items" : [ "Acoustic", "Grand", "Piano", ",", "Bright", "Acoustic", "Piano", ",", "Electric", "Grand", "Piano", ",", "Honky-tonk", "Piano", ",", "Electric", "Piano", 1, ",", "Electric", "Piano", 2, ",", "Harpsichord", ",", "Clavi", ",", "Celesta", ",", "Glockenspiel", ",", "Music", "Box", ",", "Vibraphone", ",", "Marimba", ",", "Xylophone", ",", "Tubular", "Bells", ",", "Dulcimer", ",", "Drawbar", "Organ", ",", "Percussive", "Organ", ",", "Rock", "Organ", ",", "Church", "Organ", ",", "Reed", "Organ", ",", "Accordion", ",", "Harmonica", ",", "Tango", "Accordion", ",", "Acoustic", "Guitar", "(nylon)", ",", "Acoustic", "Guitar", "(steel)", ",", "Electric", "Guitar", "(jazz)", ",", "Electric", "Guitar", "(clean)", ",", "Electric", "Guitar", "(muted)", ",", "Overdriven", "Guitar", ",", "Distortion", "Guitar", ",", "Guitar", "harmonics", ",", "Acoustic", "Bass", ",", "Electric", "Bass", "(finger)", ",", "Electric", "Bass", "(pick)", ",", "Fretless", "Bass", ",", "Slap", "Bass", 1, ",", "Slap", "Bass", 2, ",", "Synth", "Bass", 1, ",", "Synth", "Bass", 2, ",", "Violin", ",", "Viola", ",", "Cello", ",", "Contrabass", ",", "Tremolo", "Strings", ",", "Pizzicato", "Strings", ",", "Orchestral", "Harp", ",", "Timpani", ",", "String", "Ensemble", 1, ",", "String", "Ensemble", 2, ",", "SynthStrings", 1, ",", "SynthStrings", 2, ",", "Choir", "Aahs", ",", "Voice", "Oohs", ",", "Synth", "Voice", ",", "Orchestra", "Hit", ",", "Trumpet", ",", "Trombone", ",", "Tuba", ",", "Muted", "Trumpet", ",", "French", "Horn", ",", "Brass", "Section", ",", "SynthBrass", 1, ",", "SynthBrass", 2, ",", "Soprano", "Sax", ",", "Alto", "Sax", ",", "Tenor", "Sax", ",", "Baritone", "Sax", ",", "Oboe", ",", "English", "Horn", ",", "Bassoon", ",", "Clarinet", ",", "Piccolo", ",", "Flute", ",", "Recorder", ",", "Pan", "Flute", ",", "Blown", "Bottle", ",", "Shakuhachi", ",", "Whistle", ",", "Ocarina", ",", "Lead", 1, "(square)", ",", "Lead", 2, "(sawtooth)", ",", "Lead", 3, "(calliope)", ",", "Lead", 4, "(chiff)", ",", "Lead", 5, "(charang)", ",", "Lead", 6, "(voice)", ",", "Lead", 7, "(fifths)", ",", "Lead", 8, "(bass", "+", "lead)", ",", "Pad", 1, "(new", "age)", ",", "Pad", 2, "(warm)", ",", "Pad", 3, "(polysynth)", ",", "Pad", 4, "(choir)", ",", "Pad", 5, "(bowed)", ",", "Pad", 6, "(metallic)", ",", "Pad", 7, "(halo)", ",", "Pad", 8, "(sweep)", ",", "FX", 1, "(rain)", ",", "FX", 2, "(soundtrack)", ",", "FX", 3, "(crystal)", ",", "FX", 4, "(atmosphere)", ",", "FX", 5, "(brightness)", ",", "FX", 6, "(goblins)", ",", "FX", 7, "(echoes)", ",", "FX", 8, "(sci-fi)", ",", "Sitar", ",", "Banjo", ",", "Shamisen", ",", "Koto", ",", "Kalimba", ",", "Bag", "pipe", ",", "Fiddle", ",", "Shanai", ",", "Tinkle", "Bell", ",", "Agogo", ",", "Steel", "Drums", ",", "Woodblock", ",", "Taiko", "Drum", ",", "Melodic", "Tom", ",", "Synth", "Drum", ",", "Reverse", "Cymbal", ",", "Guitar", "Fret", "Noise", ",", "Breath", "Noise", ",", "Seashore", ",", "Bird", "Tweet", ",", "Telephone", "Ring", ",", "Helicopter", ",", "Applause", ",", "Gunshot" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 492.0, 269.0, 161.0, 27.0 ],
					"prefix" : "Macintosh HD:/Users/johnnyvenom/Documents/Projects/AUMI-dev/files/media/sounds/Pitched Instruments/",
					"presentation" : 1,
					"presentation_rect" : [ 555.0, 16.0, 161.0, 27.0 ],
					"style" : "",
					"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-52",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 504.0, 235.0, 87.0, 20.0 ],
					"style" : "",
					"text" : "prepend prefix"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-51",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "int" ],
					"patching_rect" : [ 492.0, 139.0, 43.0, 20.0 ],
					"style" : "",
					"text" : "t 0 s 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-50",
					"linecount" : 3,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 516.0, 175.0, 40.0, 44.0 ],
					"style" : "",
					"text" : "clear, mode $1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-44",
					"maxclass" : "newobj",
					"numinlets" : 4,
					"numoutlets" : 4,
					"outlettype" : [ "", "", "", "" ],
					"patching_rect" : [ 492.0, 111.0, 277.0, 20.0 ],
					"style" : "",
					"text" : "route /fact /user /midi"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-41",
					"maxclass" : "newobj",
					"numinlets" : 0,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 518.0, 81.0, 73.0, 20.0 ],
					"style" : "",
					"text" : "r categories"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-40",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 36.0, 758.0, 75.0, 20.0 ],
					"style" : "",
					"text" : "s categories"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-38",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 212.0, 722.0, 83.0, 20.0 ],
					"style" : "",
					"text" : "prepend /midi"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-37",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 122.0, 722.0, 85.0, 20.0 ],
					"style" : "",
					"text" : "prepend /user"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-36",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 36.0, 722.0, 81.0, 20.0 ],
					"style" : "",
					"text" : "prepend /fact"
				}

			}
, 			{
				"box" : 				{
					"fontname" : "Trebuchet MS",
					"fontsize" : 16.0,
					"id" : "obj-99",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 207.0, 416.0, 96.0, 25.0 ],
					"presentation" : 1,
					"presentation_rect" : [ 166.0, 2.0, 96.0, 25.0 ],
					"style" : "",
					"text" : "Category:",
					"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ],
					"textjustification" : 2
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-21",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 25.0, 451.0, 161.0, 20.0 ],
					"style" : "",
					"text" : "clearchecks, checkitem $1 1"
				}

			}
, 			{
				"box" : 				{
					"autopopulate" : 1,
					"bgcolor" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
					"bgfillcolor_angle" : 270.0,
					"bgfillcolor_autogradient" : 0,
					"bgfillcolor_color" : [ 0.32549, 0.345098, 0.372549, 1.0 ],
					"bgfillcolor_color1" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
					"bgfillcolor_color2" : [ 0.239216, 0.254902, 0.278431, 1.0 ],
					"bgfillcolor_proportion" : 0.39,
					"bgfillcolor_type" : "color",
					"fontname" : "Trebuchet MS",
					"fontsize" : 16.0,
					"hint" : "Choose the category of sounds you want or choose a fodler of sounds from your own computer. ",
					"id" : "obj-96",
					"items" : [ "(Default", "sounds)", ",", "Grooves", ",", "Percussion", ",", "Pitched Instruments", ",", "Sound FX", ",", "<separator>", ",", "(User)", ",", "Choose your own", ",", "<separator>", ",", "(MIDI)", ",", "AU DLS Synth 1", ",", "from Max 1", ",", "from Max 2" ],
					"maxclass" : "umenu",
					"numinlets" : 1,
					"numoutlets" : 3,
					"outlettype" : [ "int", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 25.0, 416.0, 180.0, 27.0 ],
					"prefix" : "Macintosh HD:/Users/johnnyvenom/Documents/Projects/AUMI-dev/files/media/sounds/",
					"presentation" : 1,
					"presentation_rect" : [ 264.0, 1.0, 180.0, 27.0 ],
					"style" : "",
					"textcolor" : [ 0.862745, 0.870588, 0.878431, 1.0 ]
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-33",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "int" ],
					"patching_rect" : [ 105.5, 656.0, 57.0, 20.0 ],
					"style" : "",
					"text" : "strippath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "int" ],
					"patching_rect" : [ 25.0, 552.0, 30.0, 20.0 ],
					"style" : "",
					"text" : "t b i"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-4",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 105.5, 587.0, 66.0, 20.0 ],
					"style" : "",
					"text" : "v category"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-47",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "" ],
					"patching_rect" : [ 105.5, 690.0, 131.0, 20.0 ],
					"style" : "",
					"text" : "sel \"Choose your own\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-35",
					"maxclass" : "button",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 265.0, 133.0, 24.0, 24.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-31",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 25.0, 493.0, 29.5, 20.0 ],
					"style" : "",
					"text" : "+ 1"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-29",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "int" ],
					"patching_rect" : [ 25.0, 524.0, 29.5, 20.0 ],
					"style" : "",
					"text" : ">"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-28",
					"int" : 1,
					"maxclass" : "gswitch2",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 36.0, 612.0, 88.5, 32.0 ],
					"style" : ""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-27",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 55.799999, 296.0, 335.0, 20.0 ],
					"style" : "",
					"text" : "<separator>, (User), \"Choose your own\", <separator>, (MIDI)"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-25",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 55.799999, 323.0, 98.0, 20.0 ],
					"style" : "",
					"text" : "prepend append"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-24",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 6,
					"outlettype" : [ "int", "bang", "bang", "count", "bang", "" ],
					"patching_rect" : [ 25.0, 227.0, 96.0, 20.0 ],
					"style" : "",
					"text" : "t 3 b b count b s"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-23",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 186.0, 480.0, 71.0, 20.0 ],
					"style" : "",
					"text" : "route count"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-18",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 39.5, 384.0, 67.0, 20.0 ],
					"style" : "",
					"text" : "route clear"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 39.5, 356.0, 52.0, 20.0 ],
					"style" : "",
					"text" : "midiinfo"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-43",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "bang", "bang" ],
					"patching_rect" : [ 88.5, 83.0, 34.0, 20.0 ],
					"style" : "",
					"text" : "t b b"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-78",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 25.0, 59.0, 146.0, 18.0 ],
					"style" : "",
					"text" : "set menus\n"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-17",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 25.0, 197.0, 87.0, 20.0 ],
					"style" : "",
					"text" : "prepend prefix"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-16",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 25.0, 173.0, 59.0, 20.0 ],
					"style" : "",
					"text" : "tosymbol"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-15",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 25.0, 145.0, 145.0, 20.0 ],
					"style" : "",
					"text" : "sprintf %s/media/sounds/"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"linecount" : 4,
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 185.0, 49.0, 125.5, 56.0 ],
					"style" : "",
					"text" : "\"Macintosh HD:/Users/johnnyvenom/Documents/Projects/AUMI-dev/files/\""
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-2",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 25.0, 113.0, 100.0, 20.0 ],
					"style" : "",
					"text" : "v filesFolderPath"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-3",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 25.0, 83.0, 60.0, 20.0 ],
					"style" : "",
					"text" : "loadbang"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 602.5, 167.0, 525.5, 167.0 ],
					"source" : [ "obj-1", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-60", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-1", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-39", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-10", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-16", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-15", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-17", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-16", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-24", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-17", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 97.0, 408.5, 34.5, 408.5 ],
					"source" : [ "obj-18", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-15", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-2", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-20", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 34.5, 480.0, 11.0, 480.0, 11.0, 404.0, 34.5, 404.0 ],
					"source" : [ "obj-21", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 501.5, 331.0, 477.0, 331.0, 477.0, 265.0, 501.5, 265.0 ],
					"source" : [ "obj-22", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-70", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-23", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-27", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-67", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 4 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 111.5, 252.5, 34.5, 252.5 ],
					"source" : [ "obj-24", 5 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 80.7, 287.5, 34.5, 287.5 ],
					"source" : [ "obj-24", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-24", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 65.299999, 348.0, 34.5, 348.0 ],
					"source" : [ "obj-25", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-25", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-27", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-33", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-36", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-28", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-29", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-43", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-3", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-20", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-44", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-30", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-29", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-31", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-55", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-32", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-47", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-33", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-35", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-36", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-37", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-40", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-38", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 772.166687, 228.5, 629.5, 228.5 ],
					"source" : [ "obj-39", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 712.166687, 229.5, 629.5, 229.5 ],
					"source" : [ "obj-39", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-4", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-30", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-41", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-8", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-43", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-1", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-51", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-7", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-44", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-37", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-47", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-38", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-47", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-18", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-5", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 525.5, 260.0, 501.5, 260.0 ],
					"source" : [ "obj-50", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-51", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-52", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-51", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-51", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-52", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-22", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-54", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-54", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-28", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-61", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-60", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-32", 1 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 601.5, 333.5, 572.5, 333.5 ],
					"source" : [ "obj-61", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-62", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-61", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-63", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-62", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 629.5, 260.0, 501.5, 260.0 ],
					"source" : [ "obj-63", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-96", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 96.099998, 282.5, 34.5, 282.5 ],
					"source" : [ "obj-67", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-50", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-7", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-54", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"midpoints" : [ 673.5, 261.5, 501.5, 261.5 ],
					"source" : [ "obj-7", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-2", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-21", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-23", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 2 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-31", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-4", 0 ],
					"disabled" : 0,
					"hidden" : 0,
					"source" : [ "obj-96", 1 ]
				}

			}
 ],
		"dependency_cache" : [  ],
		"autosave" : 0,
		"styles" : [ 			{
				"name" : "AudioStatus_Menu",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.294118, 0.313726, 0.337255, 1 ],
						"color1" : [ 0.454902, 0.462745, 0.482353, 0.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "myteststyle",
				"default" : 				{
					"accentcolor" : [ 0.92549, 0.364706, 0.341176, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjBlue-1",
				"default" : 				{
					"accentcolor" : [ 0.317647, 0.654902, 0.976471, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjGreen-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "newobjYellow-1",
				"default" : 				{
					"accentcolor" : [ 0.82517, 0.78181, 0.059545, 1.0 ],
					"fontsize" : [ 12.059008 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberB-1",
				"default" : 				{
					"accentcolor" : [ 0.011765, 0.396078, 0.752941, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberG-1",
				"default" : 				{
					"accentcolor" : [ 0.0, 0.533333, 0.168627, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberGold-1",
				"default" : 				{
					"accentcolor" : [ 0.764706, 0.592157, 0.101961, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "numberR-1",
				"default" : 				{
					"accentcolor" : [ 0.784314, 0.145098, 0.023529, 1.0 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "panelGold-1",
				"default" : 				{
					"bgfillcolor" : 					{
						"type" : "color",
						"color" : [ 0.764706, 0.592157, 0.101961, 0.25 ],
						"color1" : [ 0.764706, 0.592157, 0.101961, 1.0 ],
						"color2" : [ 0.290196, 0.309804, 0.301961, 1.0 ],
						"angle" : 270.0,
						"proportion" : 0.39,
						"autogradient" : 0
					}

				}
,
				"parentstyle" : "",
				"multi" : 0
			}
, 			{
				"name" : "texteditGold",
				"default" : 				{
					"textcolor_inverse" : [ 0.0, 0.0, 0.0, 1.0 ],
					"bgcolor" : [ 0.764706, 0.592157, 0.101961, 0.68 ]
				}
,
				"parentstyle" : "",
				"multi" : 0
			}
 ]
	}

}
