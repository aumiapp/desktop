AUMI v4 User Guide
==================

## Audience analysis

- educators
- music therapists
- occupational therapists
- academic researchers

## Design coordination

- Write in markdown, export to pdf and html
- Can be formatted into nice printable version later. 

## Task analysis

1. Download and install
	1. Mac/Windows
2. Operate the application
	1. Camera
		1. Operation
		2. Full screen 
		3. Settings
	2. Interaction
		1. Zone tracker
		2. Radial tracker
		3. Relative motion 
	3. Instrument
		1. Keyboard module
		2. Sampler module
3. Manage presets
	1. Presets and storage
	2. Importing and exporting
	3. Using AUMI with groups

## Manual Layout

- Table of Contents
- Introduction
	- What's new in v4
- Download and installation
- Main Interface description 
	+ Main areas: 
		1. Control Panel
		2. Interaction
		3. Instrument
		4. Video Display
		5. Title Panel
		6. Menu bar - Audio 
- Getting Started
	+ Starting/adjusting camera
	+ Selecting Interaction Module
	+ Selecting Sounds Module
	+ Aligning tracking dot
	+ Help and help menus
- Components
	+ Camera setup and settings
		* Selection
		* Fullscreen
		* Settings 
	+ Interaction Modules
		* Zone Tracker
		* Radial Tracker 
		* Relative Movement
	+ Sounds
		* Keyboard
		* Sampler 
	+ Presets 
		* Load
			- Free mode
		* Save
		* More
			- Import
			- Export
			- Delete
- Best Practices and Tips (Key Points to Using the AUMI Software)
- Troubleshooting
- Technical Support and Contact Information
- Appendix: 
	+ Quick Start Guide