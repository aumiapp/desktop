AUMI v4 - development notes
===========================

#### Newest version: v4.1.2 2021-08-27

### Branches:

There are currently **4** branches (current branch in **BOLD**): 


- **master** (latest release v4.1.2: working, distributable versions for Mac and Windows) 
- development - re-opened dev branch for May 2019
- windows-fix - May 2019, fix for [Issue 26](https://gitlab.com/johnnyvenom-phd/AUMI-desktop/issues/26), released a 4.1.2 on 2020-01-01.
- ultrahaptics - experimental features specifically for UltraHaptics Development Kit. 

---

![AUMI](documentation/media/AUMI.png)

This is an ongoing list of current work on the AUMI desktop application project. The technical development is being done by John Sullivan at the [Input Devices and Music Interaction Laboratory][1] at McGill University. For more information on the project, contact [john.sullivan2@mail.mcgill.ca][2]. 

More information about the project can be found [here][3].

[1]: http://idmil.org "IDMIL website"
[2]: mailto:john.sullivan2@mail.mcgill.ca "John Sullivan email"
[3]: http://aumiapp.com/ "AUMI website"

<hr>

**Table of Contents:**

<!-- MarkdownTOC -->

- [AUMI v4 - development notes](#aumi-v4---development-notes)
      - [Newest version: v4.1.2 2021-08-27](#newest-version-v412-2021-08-27)
    - [Branches:](#branches)
  - [Changelog](#changelog)
  - [Compiling standalone applications](#compiling-standalone-applications)
  - [Development](#development)
    - [ToDos (from 2016):](#todos-from-2016)
    - [Feature Requests](#feature-requests)
    - [General information and troubleshooting](#general-information-and-troubleshooting)
  - [Other notes](#other-notes)
    - [Meeting Eric Jul 27 2017](#meeting-eric-jul-27-2017)
    - [Walkthrough of new AUMI application.](#walkthrough-of-new-aumi-application)
    - [Feb 2016 - Skype meeting w/ Leaf/Ivan](#feb-2016---skype-meeting-w-leafivan)
    - [Questions for Ivan:](#questions-for-ivan)
  - [Development Notes](#development-notes)
    - [4 May 2016](#4-may-2016)
    - [Notes - Skype meeting - Leaf 10-May-2016](#notes---skype-meeting---leaf-10-may-2016)
    - [Ivan -](#ivan--)

<!-- /MarkdownTOC -->


<hr>

## Changelog
All notable changes to this project will be documented here. 
Version number follows [Semantic Versioning](http://semver.org/) guidelines.

```
# [Unreleased]

# [4.1.2] (MacOS) - 2021-08-27
## Fixed: 
- Recompiled standalone application to run with MacOS 11 Big Sur. Under Big Sur the menus on the previous version were not functional. 

# [4.1.2] (Windows) - 2019-05-01
## Fixed: 
- Updated code that was causing Windows standalone to freeze when loading. 

# [4.1.1] - 2017-10-20
## Added
- AUMI 4 User Guide

## Fixed
- Fixed a bug where camera settings were not getting saved in presets.

# [4.1.0] - 2017-07-26
## Changed
- Presets:
    - General: now user can load, save, rename, and delete presets. Also added 'free mode' where settings are autosaved within session. 
    - Note sequences: in Keyboard module, sequences are now saved with preset. 
    - Sampler: Now samples are saved with preset, including user-loaded samples.
    - User can import and export presets from/to file.
- Moved 'Audio settings' menu to menubar, and removed Max Console from standalone (Mac).
- UI update (along with menu optimizations), minor tweaks and bugfixes.

## Added
- - New keyboard module that can play back sample libraries, iPad sound libraries, user sounds, and MIDI.

## Fixed
- Fixed a bug where app was very slow to load on Mac. 
- Relative motion tracking - implemented edge restraint behavior and user options.
- Fixed a bug where the keyboard help window was blank. (2017-07-27)
- Fixed a bug in the keyboard module where the scales were not updating correctly when the root note was changed. (2017-07-27)

# [4.0.1b] - 2016-08-03
## Changed
- Updated Edge Tracking options (formerly "Dot Tracking"). Now default mode is Recenter, with a 2 second delay before it recenters. 

# [4.0.1a] - 2016-06-04
## Added
- Added zoom functionality with - and = keys, 0 to return to 100%

## Changed
- Reduced application height to 700 to accommodate default Windows display size (and taskbar)

## Fixed
- Fixed a bug in compiling Windows standalone versions so now cameras work properly. 

# [4.0.1] - 2016-05-18
## Added
- Dot tracking options under Appearance
- Save settings for MIDI melody module
- Quick Start button when modules aren't selected

## Changed
- Reorder lefthand options to chronological
- Updated help files
- Troubleshooting Windows 10 functionality


# [4.0.0] - 2016-02-01
## Changed
- Move camera module to top of interface
- Increase visibility for visual settings
- Implemented limited color palette

```

## Compiling standalone applications

Follow these steps to compile standalone applications. 

- OSX:
    - On a Mac PC, compile application in Max (File > Build Collective / Application) using `macbuild.txt` script. Make sure appicon path points to a valid .ico file.
    - Add `/files` folder to `AUMI.app/Contents/Resources`
    - [standalone] object options: 
        + <img src="./documentation/media/standalone_options_mac.jpg" width="400">
        +  *Failure to follow these may cause the application to crash or not function properly.*
    - To edit Mac Console menu option, edit the `maxinterface.json` file in `AUMI.app/Contents/Resources/C74/interfaces` folder.
        + <img src="./documentation/media/maxinterface_edit.jpg" width="400">
        + **Better:** To remove entirely, comment out lines 2857 - 2861. Note: must leave line 2856 or app will not load. 
    - Users must disable gatekeeper feature to allow OSX to open new downloaded applications (unless app is signed). For the release, Henry Lowengard can sign the app for distribution. 
- Windows:   
    - On a Windows PC, compile application in Max (File > Build Collective / Application) using `winbuild.txt` script. Make sure appicon path points to a valid .ico file.
    - Add `/files` folder to the application root folder, alongside the .exe, .mxf, and `/resources` folder. 
    - Use the following [standalone] object options:
        + <img src="./documentation/media/standalone_options_windows.jpg" width="400">
        +  *Failure to follow these may cause the application to crash or not function properly.*
    - **THIS STEP DISCONTINUED. Now only compiling single 32bit version for Windows.** FOR NOW, I AM COMPILING BOTH 64 AND 32 BIT VERSIONS. Previously, Ivan was only making 32 bit. 
        - Advantages to having both 64 & 32?
    - **THE FOLLOWING STEPS ARE NO LONGER NEEDED WITH THE MORE RECENT MAX 7 UPDATES** 
        + Add `VIDDLL` packages to both 32 and 64 bit versions. Instructions for each:
            - 64: Go to `Program Files/Cycling '74/Max 7/resources/packages` and copy `VIDDLL` directory to the compiled standalone bundle here: `~/resources/packages`
            - 32: Go to `Program Files (x86)/Cycling '74/Max 7/resources/packages` and copy `VIDDLL` directory to the compiled standalone bundle here: `~/resources/packages`
        + Windows standalone requires additional helper files. Add them to the zip distribution along with instructions. 
            - For more information: https://cycling74.com/forums/topic/standalone-missing-msvcp120-dll-on-windows/#.Vzzj-JMrLXE

## Development 

Recent development, bugfixes and documentation has been kept on the GitHub repository at https://github.com/johnnyvenom/AUMI-desktop. See the Issues tab for detailed notes. 

### ToDos (from 2016):
**Items listed by priority (most important first)**

1. **COMLETED:** Move camera module to top of interface (1st thing) 
1. **COMLETED:** Increase visibility for 'Visual Settings' 
1. **COMLETED:** Limited color palette 
	- Monochrome with different color headers for each module
1. Implement and improve User preset system.
	- **COMPLETED** Saves settings for MIDI Melody module.
	- **COMPLETED**Need to implement for sampler
	- **updated** Changes are saved in-application in realtime, written to presets file on application close. NOW: presets are saved by user; in "free mode" settings are autosaved.
	- Sampler: save samples (including user-added samples)
	- Also see [Feature Requests](#feature-requests) section #s 1 and 2.
1. **COMPLETED** Windows 10 testing. 
	- **COMPLETED** Have tested with Win 10 and confirmed that it is working, however there are helper files that need to be installed. (See installation instructions included with distributions and compiling instructions below)
    - **FIXED** ~Update~ There is a problem with compiling standalone Windows app - camera not recognized. See [Github issue #12](https://github.com/johnnyvenom/AUMI-desktop/issues/12)
1. Module for color skins for the application (future)
	- Can we use the built-in style generator? ANSWER: not for all elements.
	- Or, create a send/receives (or scripting) to update the main visual elements (bgcolor, accent color, fonts - something like that)
1. MediaWiki re-install (Thomas/Detta)
1. Apple sign app, to get past gatekeeper. For release version.  
	- What Apple Developer account should we use to sign? RPI?
	- https://developer.apple.com/osx/distribution/
	- https://developer.apple.com/developer-id/
    - For the time being, Henry can sign the app without issue. 

### Feature Requests
**These items should be discussed before working on them.**

1. **COMPLETED** Save New Sequences (instrument_bpatcher)? (Lise, 2-Mar-2016)
	- Need to continue working with preset system. Not generally implemented with any of the sound module settings. 
	- Separate level save function (save a new sequence) from general user preset saving, that can then be accessed from other user presets. 
1. **COMPLETED** Save new samples & sampler settings with User preset
	- Need to continue working with preset system. Not generally implemented with any of the sound module settings. 
1. Play along with iTunes (Lise, 2-Mar-2016)
	- Never existed, no plans for implementation. 
1. **COMPLETED** Tracking dot edge boundary stop, or re-centering (Julia, 16-Mar-2016)
	- The main concern I have is about the tracking dot. Some of our enthusiastic players at the Arts Center often go off screen, which causes the tracking dot to completely disappear in this version, and it does not reset until the mouse is clicked in the tracking window. Would it be possible to have the dot reset itself, either at the edge where it left off, or in the center of the screen? That way, the student can continue playing -- the tracking may be inaccurate until it's properly reset, but it's better than nothing!

### General information and troubleshooting

- Problem: Unable to open newly downloaded software on Mac (Leaf, 16-Feb-2016)
	- Resolution: Must remove old version from directory so that newly downloaded version is named `AUMI`, not something like `AUMI 2`. 

<hr>

***{Can mostly ignore from here on...}***

## Other notes

### Meeting Eric Jul 27 2017

- Ideas: 
    + Sample start from where it stopped instead of beginning every time
        * One shot plays through whole sample
        * Stops when new sample triggered
        * Can return to point it stopped at. 
    + Simple audio editor
- **FIXED** KEYBOARD CONSOLE NOT LOADING
- **FIXED** KEYBOARD SCALE TYPES NOT CORRECT. 

### Walkthrough of new AUMI application. 
**Ivan - 26-Jan-2016**
- L-Shaped layout - order of initializing instrument
	- Vertical - controls
	- Horizontal - instrumental controls
- Modular - help screens - driven from text files
- Presets: 
	- Check out pattrstorage and deferlow functionality (need to use deferlow in bpatchers)
- ToDo: Add send and receives on the list of s/r’s

**Links:**
- http://ccc-rpi.org/research/aumi (newer)
- http://deeplistening.org/site/adaptiveuse

### Feb 2016 - Skype meeting w/ Leaf/Ivan

- AUMI since 2007
- Leaf needs a most up to date version 4.0

Questions: 
- What OS is necessary? 
    - OSX 10.7 and later
    - Windows 7 and later
- 3.1 version still available?

Leaf’s notes for new version: 
- Put camera module above presets
    - Need a simple ‘ON’ button? Could we put it 
- Leaf’s presets aren’t working
- Rename ‘Interaction’
- ‘Visual’ should be in orange - maybe 1st level instead of 2nd - make more visible?
- Press ‘space’ to auto-center the dot - connect it to when the dot falls off the screen. 

- Get iPad version?
- Get 3.0 version? 

### Questions for Ivan: 

- Presets: 
    - Exactly what parameters are/aren’t saved? 
    - Seems like Interaction/Sounds modules aren’t loaded by default. 
    - What is the ’Save’ action? Or does it simply save each new parameter change? 
    - a new presetfile.json is saved at the root level. Is this a copy of the same in ./files/preset/ ?
- Sometime dot isn’t present. 
- AUMI.app.zip is what? Is it needed? 
- When opening bundled application, there are errors in the Max console but app seems to run fine. (This from your last distributions)
- When interaction is on ‘none’ and you switch to fullscreen, then turn a tracker on, it doesn’t show up. 

- Compiling for Windows: 
    - [standalone] inspector 
        - uncheck top-level patcher
        - check the other 2 options
- put usernames.txt in application support (in presetmenu subpatcher) 

<hr>
<hr>

## Development Notes

### 4 May 2016

Item: Improve and expand presets
- Problem: Crashes Max when reloading previously altered presets during a session. 
	- Cause: `presetfile.json` is not getting updated, so Max's cached preset settings don't match with json settings? Maybe it is trying to load from the json file? 
	- Fix: Update json file with every change? Could create [s save] and [r save] objects to save from anywhere.
- Preset saving architecture: overwrites currently selected preset. Maybe rewrite this so you could select a new slot if it is already written to.

**Changes:**
- Updated Username saving to retain original username
	- see above (preset saving architecture), or add pop-up confirmation to overwrite/*create new preset*

	pseudocode: 
		- Click main patcher to save and dialog comes up:
		"Update preset or create new?"
		Update = same as now
		New = create new ID
			- if all full = dialog "All slots are full, please choose a slot to overwrite"
- Added presets to MIDI Melody

### Notes - Skype meeting - Leaf 10-May-2016

- A little overwhelming for the new non-technical user... 
	- the new layout is better.
	- Can it be even simpler?
- **COMPLETED** Dot disappearing... keep within the border? 
	- There are 2 options: 
		- When blob goes off screen it can reset to center again... or
		- keep at the border
	- iPad version reorients to the center
- **COMPLETED** I can get iPad version from Henry - email him. 
- **COMPLETED** Bring sounds from ver 3.1 to new version
	- Where are these sounds from? 
- **COMPLETED** Startup - user interaction:
	- Instant playability: 
		- Incorporate a "turn everything on" button at the top
		- or, make a default preset at top of presets
		- or make it automatic everything on
	- Maybe move the LH column order to:
		- AUMI - About/Get Started
		- "ALL ON" button
		- presets
		- camera
		- interaction
		- sounds

**Next Steps**
- Do now: 
	- **COMPLETED** - sounds from 3.1?
	- **COMPLETED** - Default configuration
	- **COMPLETED** - MIDI melody presets (already done)
	- **COMPLETED** - Move visual settings back to small 
	- New release version to team
	- **COMPLETED** = get iPad version for comparison
- After: 
	- Presets: 
		- Sampler
		- Melody - new sequence
	- Sampler: 
		- load new sample from umenu
		- Make default instruments instead of random load
	- Offscreen tracking behavior
	- app signing

### Ivan - 

- presets MIDI melody - save new sequence instead of scales/octave/root
- save melodies instead of seq/scale/etc presets
	- use a 2nd level preset system for melodies - each user can have multiple melodies
- sampler
	- name 1-shots w/ javascript then they can be individually addressed
	- look at midi melody number boxes for dynamic naming
	- sampler contains the preset 
	- save user samples by saving absolute filepath,,
	- or save actual files within max application (create file upload interface)
- dot: 
	- spacebar resets dot to center
	- hidden timeout recenter option too
- saving presets:
	- autosave preset every 2 seconds (updates pattrstorage table)
	- saves last preset when preset is changed (updates pattrstorage table)
	- writes file to disk when application is closed. 






