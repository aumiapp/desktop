ZONE TRACKER
To start click the point you want to track on the video image.

Play sounds by making the colored dot cross to a different division of the rectangular area.

Using the onscreen controls you can choose between an horizontal or a vertical orientation, define the total number of hot zones and resize the total tracking area.

In "Appearance" you can change the style and behavior of the tracking dot and guides. 

By default, when the point that is being tracked moves outside of the display area, the dot will remain at the edge of the screen and attempt to reconnect to the tracking point if it comes back into the camera's field of vision. After a short time, if it cannot reconnect, the dot will reset to the center of the screen. Alternately, the default behavior can be changed with the "Edge Tracking" options under the "Appearance" options. At any time the dot can be recentered by pressing the spacebar.  
