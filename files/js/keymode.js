inlets = 2;
outlets = 1;

var current; // selected category from umenu
var defaults, user, gm, external; // option types
var mode;

function msg_int(v) {
	if (inlet == 1) {
		defaults = v;
	}
	
	if (inlet == 0) {
		current = v + 1;
		setMode(current, defaults);
	}
}

function setMode(cur, def) {
	user = def + 3;
	gm = def + 6; 
	external = def + 7;

	if (cur <= def) {
		mode = 1;
	} else if (cur == user) {
		mode = 2; 
	} else if (cur == gm) {
		mode = 3; 
	} else if (cur == external) {
		mode = 4;
	} else {
		mode = 0;
	}

	// post("mode: ", mode); 

	outlet(0, mode);
}

function in0text() {
	assist("takes the index of current umenu item");
}
function in1text() {
	assist("takes the count of default sound folders");
}
function out0text() {
	assist("outputs the mode: 1 - defaults, 2 - user, 3 - GM, 4 - external MIDI");
}

setinletassist(0, in0text);
setinletassist(1, in1text);
setoutletassist(0, out0text);
